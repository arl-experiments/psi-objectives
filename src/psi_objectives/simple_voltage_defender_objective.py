import logging
from typing import Dict, Optional

from palaestrai.agent.memory import Memory
from palaestrai.agent.objective import Objective
from .gauss import normal_distribution_pdf

LOG = logging.getLogger("palaestrai.agent.objective")


class SimpleVoltageBandPendulumDefender(Objective):
    VM_PU_LOW = 0.98
    VM_PU_HIGH = 1.02
    SIGMA = 0.032
    C = 0.0
    A = 10.0

    def __init__(self, params: Optional[Dict] = None):
        params = {} if not params else params
        super().__init__(params)
        self._sigma = params.get("sigma", SimpleVoltageBandPendulumDefender.SIGMA)
        self._c = params.get("c", SimpleVoltageBandPendulumDefender.C)
        self._a = params.get("a", SimpleVoltageBandPendulumDefender.A)
        self.step = 0

    def internal_reward(self, memory: Memory, **kwargs):
        """Expect Voltage values and reward voltage band violations."""
        vm_pu = float(
            memory.get_rewards()[-1:].filter(
                like="vm_pu-median", axis=1
            ).iloc[-1]
        )

        objective = normal_distribution_pdf(
            x=vm_pu,
            mu=1.0,
            sigma=0.032,
            c=0.0,
            a=10.0
            )

        self.step += 1
        return objective
