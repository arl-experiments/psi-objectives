from typing import TYPE_CHECKING, List

from palaestrai.agent import Objective

if TYPE_CHECKING:
    from palaestrai.agent import RewardInformation

class PsiDummyObjective(Objective):
    def __init__(self):
        pass

    def internal_reward(self, rewards: List["RewardInformation"], **kwargs) -> float:

        renewable_energy = 0
        fossil_energy = 0
        for rew in rewards:
            if rew.reward_id == "renewable_energy":
                renewable_energy = rew.reward_value
            if rew.reward_id == "fossil_energy":
                fossil_energy = rew.reward_value

        value = renewable_energy - fossil_energy
        print(value)
        return value
